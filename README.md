# README #

# server repo: https://bitbucket.org/GabeEddyT/grand-theft-artist-server

# prebuilt game: [GTArt.zip](https://bitbucket.org/GabeEddyT/grand-theft-artist/downloads/GTArt.zip)

What started out as Gabe's final game in Production I -- now networked. 
If you actually build this, keep in mind this was originally designed 
around a beloved steering wheel controller known simply as, "Thrustmaster". 
As such, the first menu won't make sense; just press Enter.